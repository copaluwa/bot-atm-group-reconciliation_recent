﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace RateHistoryExtraction
{
    public class Operations
    {
        
        private Logger logger = new Logger();
        public Operations(Logger _logger)
        {
            logger = _logger;
        }
        public async Task<bool> BulkCopyToDest2(CancellationToken cancellationToken, string query, int timeout, string destinationTable, string srcConn, string destCon)
        {
            //var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            //var connectionII = @"Server=(LocalDB)\BluePrismLocalDB;Initial Catalog=BluePrism;Trusted_Connection=True;";
            bool output = false;
            try
            {
                using (OracleConnection readConn = new OracleConnection(srcConn))
                {
                    using (SqlConnection bulkCopy = new SqlConnection(destCon))
                    {

                        // Note that we are using the same cancellation token for calls to both connections\commands
                        // Also we can start both the connection opening asynchronously, and then wait for both to complete
                        logger.Info("Opening connection to Database");
                        Task openReadConn = readConn.OpenAsync(cancellationToken);
                        //Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                        await Task.WhenAll(openReadConn);
                        DateTime? nodate = null;
                        string sql = $"INSERT INTO {destinationTable} (BRANCH_CODE,CCY1,CCY2,RATE_TYPE,MID_RATE,BUY_RATE,SALE_RATE,RATE_DT_STAMP,RATE_DATE) " +
                            $"Values (@BRANCH_CODE, @CCY1, @CCY2, @RATE_TYPE, @MID_RATE , @BUY_RATE, @SALE_RATE,@RATE_DT_STAMP, @RATE_DATE);";
                        OracleCommand readCmd = new OracleCommand(query, readConn);
                        try
                        {
                            var sourceData = new DataSet();
                            logger.Info("Reading Data from source Database");
                            readCmd.CommandTimeout = timeout;
                            var reader = readCmd.ExecuteReader(CommandBehavior.SequentialAccess);
                            reader.FetchSize = reader.RowSize * 1000;
                            
                            while (reader.Read())
                            {
                                var model = new RateModel();
                                model.BRANCH_CODE = reader["BRANCH_CODE"] != DBNull.Value ? (string)reader["BRANCH_CODE"] : "";
                                //logger.Info(model.BRANCH_CODE);
                                model.CCY1 = reader["CCY1"] != DBNull.Value ? (string)reader["CCY1"] : "";
                                model.CCY2 = reader["CCY2"] != DBNull.Value ? (string)reader["CCY2"] : "";
                                model.RATE_TYPE = reader["RATE_TYPE"] != DBNull.Value ? (string)reader["RATE_TYPE"] : "";
                                model.MID_RATE = reader["MID_RATE"] != DBNull.Value ? (decimal)reader["MID_RATE"] : 0;
                                //logger.Info(model.MID_RATE.ToString());
                                model.BUY_RATE = reader["BUY_RATE"] != DBNull.Value ? (decimal)reader["BUY_RATE"] : 0;
                                //logger.Info(model.BUY_RATE.ToString());
                                model.SALE_RATE = reader["SALE_RATE"] != DBNull.Value ? (decimal)reader["SALE_RATE"] : 0;
                                //logger.Info(model.SALE_RATE.ToString());
                                //model.RATE_SERIAL = reader["RATE_SERIAL"] != DBNull.Value ? (string)reader["RATE_SERIAL"] : "0";
                                //logger.Info(model.RATE_SERIAL.ToString());
                                model.RATE_DT_STAMP = reader["RATE_DT_STAMP"] != DBNull.Value ? Convert.ToString(reader["RATE_DT_STAMP"]) : "";
                                //logger.Info(model.RATE_DT_STAMP.ToString());
                                model.RATE_DATE = reader["RATE_DATE"] != DBNull.Value ? (DateTime)reader["RATE_DATE"] : nodate;
                                //logger.Info(model.RATE_DATE.ToString());
                                //logger.Info($"BRN: {model.BRANCH_CODE}  MID RATE: {model.MID_RATE}   RATE_DT_STAMP {model.RATE_DT_STAMP}");
                                bulkCopy.Execute(sql, model);
                                //await bulkCopy.InsertAsync(model);
                            }

                            logger.Info("Writing Data to destination Database");

                            output = true;
                        }
                        catch (Exception ex)
                        {

                            if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                            {
                                string pattern = @"\d+";
                                Match match = Regex.Match(ex.Message.ToString(), pattern);
                                var index = Convert.ToInt32(match.Value) - 1;

                                FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                                var sortedColumns = fi.GetValue(bulkCopy);
                                var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                                FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                                var metadata = itemdata.GetValue(items[index]);

                                var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                logger.Info(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                            }

                            logger.Error(ex);

                            output = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }
            return output;
        }
    }
}
