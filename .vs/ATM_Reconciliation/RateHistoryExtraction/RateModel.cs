﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateHistoryExtraction
{
	[Table("RATES_HISTORY")]
    public class RateModel
    {
		[Key]
		public string BRANCH_CODE { get; set; }
		public string CCY1 { get; set; }
		public string CCY2 { get; set; }
		public string RATE_TYPE { get; set; }
		public string RATE_DT_STAMP { get; set; }
		public decimal MID_RATE { get; set; }
		public decimal BUY_RATE { get; set; }
		public decimal SALE_RATE { get; set; }
		//public string  RATE_SERIAL { get; set; }
		public DateTime?  RATE_DATE { get; set; }

	}
}
