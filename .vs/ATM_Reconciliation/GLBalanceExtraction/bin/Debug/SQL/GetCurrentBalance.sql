﻿SELECT *
	FROM (
		SELECT ac_ccy,trn_dt,
		SUM(decode(drcr_ind,
		'D',
		decode(ac_ccy, '{CCY}', -lcy_amount, -fcy_amount),
		decode(ac_ccy, '{CCY}', lcy_amount, fcy_amount)))   over(PARTITION BY ac_no, ac_ccy ORDER BY trn_dt) AS "RUNNING BALANCE"
			FROM {Prefix}.acvw_all_ac_entries_new
			WHERE trn_dt between  '{StartDate}' and '{EndDate}'
			AND ac_no IN ('{ACCT}')
			AND event NOT IN ('REVL')
			AND ac_ccy = '{CCY}'
			AND ac_branch IN ('{BRANCH}')
		ORDER BY trn_dt DESC
		) WHERE rownum = 1;