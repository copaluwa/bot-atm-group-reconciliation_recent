﻿UPDATE [dbo].[ATM_TERMINAL_LIST_GROUP_RECON]
   SET [OPENING_BAL] = @OpeningBalance,
	   [CLOSING_BAL] = @ClosingBalance
	   [STATUS] = @Status
	WHERE [ACCT] = @Acct
	AND [BRANCH_CODE] = @BranchCode
	AND [TERMINAL_ID] = @TerminalId
	AND [CLUSTER] = @Cluster