﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLBalanceExtraction
{
    public class OutputRecords
    {
        public string AcCcy { get; set; }
        public Nullable<DateTime> TrnDate { get; set; }
        public decimal RunningBalance { get; set; }
    }
}
