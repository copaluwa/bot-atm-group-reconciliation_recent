﻿UPDATE [dbo].[ATM_TERMINAL_LIST_GROUP_RECON]
   SET [RUNNING_BAL] = @RunningBalance,
	   [CLOSING_BAL] = [CLOSING_BAL] + @RunningBalance,
	   [STATUS] = @Status
	WHERE [ACCT] = @Acct
	AND [BRANCH_CODE] = @BranchCode
	AND [TERMINAL_ID] = @TerminalId
	AND [CLUSTER] = @Cluster