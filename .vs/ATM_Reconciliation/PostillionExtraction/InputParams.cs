﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostillionExtraction
{
    class InputParams : ParamsObject
    {
        public InputParams(string[] args)
          : base(args)
        {

        }
        [Switch("SD")]
        public string StartDate { get; set; }

        [Switch("DT")]
        public string DateUsed { get; set; }
    }
}
