﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PostillionExtraction
{
    public class StreamData
    {
        public  static async Task E2EStream(CancellationToken cancellationToken)
        {
            var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            var connectionII = @"Server=(LocalDB)\BluePrismLocalDBS;Initial Catalog=BluePrism;Trusted_Connection=True;";
            
            using (SqlConnection readConn = new SqlConnection(connection))
            {
                using (SqlConnection writeConn = new SqlConnection(connectionII))
                {

                    // Note that we are using the same cancellation token for calls to both connections\commands
                    // Also we can start both the connection opening asynchronously, and then wait for both to complete
                    Task openReadConn = readConn.OpenAsync(cancellationToken);
                    Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                    await Task.WhenAll(openReadConn, openWriteConn);

                    using (SqlCommand readCmd = new SqlCommand("SELECT [bindata] FROM [BinaryStreams]", readConn))
                    {
                        using (SqlCommand writeCmd = new SqlCommand("INSERT INTO [BinaryStreamsCopy] (bindata) VALUES (@bindata)", writeConn))
                        {

                            // Add an empty parameter to the write command which will be used for the streams we are copying
                            // Size is set to -1 to indicate "MAX"
                            SqlParameter streamParameter = writeCmd.Parameters.Add("@bindata", SqlDbType.Binary, -1);

                            // The reader needs to be executed with the SequentialAccess behavior to enable network streaming
                            // Otherwise ReadAsync will buffer the entire BLOB into memory which can cause scalability issues or even OutOfMemoryExceptions
                            using (SqlDataReader reader = await readCmd.ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken))
                            {
                                while (await reader.ReadAsync(cancellationToken))
                                {
                                    // Grab a stream to the binary data in the source database
                                    using (Stream dataStream = reader.GetStream(0))
                                    {

                                        // Set the parameter value to the stream source that was opened
                                        streamParameter.Value = dataStream;

                                        // Asynchronously send data from one database to another
                                        await writeCmd.ExecuteNonQueryAsync(cancellationToken);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public static async Task BulkCopyToDest(CancellationToken cancellationToken, string query,int timeout, string destinationTable, string srcConn, string destCon)
        {
            //var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            //var connectionII = @"Server=(LocalDB)\BluePrismLocalDB;Initial Catalog=BluePrism;Trusted_Connection=True;";

            using (SqlConnection readConn = new SqlConnection(srcConn))
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destCon))
                {

                    // Note that we are using the same cancellation token for calls to both connections\commands
                    // Also we can start both the connection opening asynchronously, and then wait for both to complete
                    Task openReadConn = readConn.OpenAsync(cancellationToken);
                    //Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                    await Task.WhenAll(openReadConn);


                    using (SqlCommand readCmd = new SqlCommand(query, readConn))
                    {
                        SqlDataReader reader = await readCmd.ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken);

                        Console.WriteLine("Inserting to Database");
                        bulkCopy.DestinationTableName = destinationTable;
                        bulkCopy.BulkCopyTimeout = timeout;
                        bulkCopy.WriteToServer(reader);
                      
                    }
                }
            }
        }
    }
}
