﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace PostillionExtraction
{
    class Program
    {
        static Logger logger = new Logger();
        static void Main(string[] args)
        {
            
            //Load connections;
            logger.Info("Extraction Started");
            logger.Info("Loading Configuration");
            var sourceConnection = "sourceConnectionString".GetKeyValue();
            var destinationConnection = "destConnectionString".GetKeyValue();
            var destinationTable = "destinationTable".GetKeyValue();
            int timeOut = Convert.ToInt32("timeout".GetKeyValue());
            string resultPath = "resultPath".GetKeyValue();
            bool outcome = false;
            string baseFolder = AppDomain.CurrentDomain.BaseDirectory;

            InputParams inputParams = new InputParams(args);
            //Load sql script;
            try
            {
                string query = File.ReadAllText($"{baseFolder}\\script.txt");
                query = query.Replace("{StartDate}", inputParams.StartDate);
                query = query.Replace("{TransDate}", inputParams.DateUsed);
                outcome = BulkCopyToDest(CancellationToken.None, query, timeOut, destinationTable, sourceConnection, destinationConnection).Result;

            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }

            WriteOutput(outcome, resultPath);

            //var dTable = dataTable();
            //Console.WriteLine("Writing records to memory");
            //for (int i = 0; i <= 1000000; i++)
            //{
            //    DataRow workRow = dTable.NewRow();
            //    workRow["retrieval_reference_nr"] = "123456789012";
            //    workRow["system_trace_audit_nr"] = "12";
            //    workRow["datetime_tran_local"] = DateTime.Now;
            //    workRow["pan"] = "123456789123";
            //    workRow["from_account_id"] = "1234565432";
            //    workRow["to_account_id"] = "1234567654";
            //    workRow["tran_type"] = "12";
            //    workRow["message_type"] = "123";
            //    workRow["settle_amount"] = 100000;
            //    workRow["tran_amount"] = 10000;
            //    workRow["tran_fee"] = 200;
            //    workRow["sink_node_name"] = "APSink";
            //    workRow["card_acceptor_name_loc"] = "locker";
            //    workRow["terminal_id"] = "123456";
            //    workRow["rsp_code_rsp"] = "00";
            //    workRow["settle_currency_code"] = "NGN";
            //    workRow["tran_currency_code"] = "NGN";
            //    workRow["card_product"] = "mastercard";
            //    workRow["sponsor_bank"] = "ecobank";
            //    workRow["datetime_tran_gmt"] = DateTime.Now;
            //    workRow["datetime_req"] = DateTime.Now;
            //    workRow["datetime_rsp"] = DateTime.Now;
            //    workRow["realtime_business_date"] = DateTime.Now;
            //    workRow["recon_business_date"] = DateTime.Now;
            //    workRow["terminal_owner"] = "Nigeria";
            //    workRow["tran_reversed"] = "0";
            //    dTable.Rows.Add(workRow);
            //    if (i % 1000000 == 0)
            //    {
            //        Console.WriteLine($"{i} records written to memory");
            //    }
            //}
            //Console.WriteLine("Writing to memory completed");
            //var done = InsertIntoDB(dTable, sourceConnection);
            //if (done)
            //    Console.WriteLine("Writing to database completed");
            //else
            //    Console.WriteLine("Writing to database failed");

            
        }

        public static bool InsertIntoDB(DataTable dataTable, string srcConnection)
        {
            bool IsSuccessful = false;
            Console.WriteLine("Writing to database");
            var connection = @srcConnection;
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
            {
                try
                {
                    Console.WriteLine("Inserting to Database");
                    bulkCopy.DestinationTableName = "POSTILION_GROUP_A";
                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.WriteToServer(dataTable);
                    IsSuccessful = true;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                    {
                        string pattern = @"\d+";
                        Match match = Regex.Match(ex.Message.ToString(), pattern);
                        var index = Convert.ToInt32(match.Value) - 1;

                        FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                        var sortedColumns = fi.GetValue(bulkCopy);
                        var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                        FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                        var metadata = itemdata.GetValue(items[index]);

                        var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                        var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                        throw new Exception(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                    }

                    throw;
                }
            }

            return IsSuccessful;

        }
        public static DataTable dataTable()
        {
            DataTable dt = new DataTable("Postillion");
            dt.Columns.Add("retrieval_reference_nr", typeof(string));
            dt.Columns.Add("system_trace_audit_nr", typeof(string));
            dt.Columns.Add("datetime_tran_local", typeof(DateTime));
            dt.Columns.Add("pan", typeof(string));
            dt.Columns.Add("from_account_id", typeof(string));
            dt.Columns.Add("to_account_id", typeof(string));
            dt.Columns.Add("tran_type", typeof(string));
            dt.Columns.Add("message_type", typeof(string));
            dt.Columns.Add("settle_amount", typeof(decimal));
            dt.Columns.Add("tran_amount", typeof(decimal));
            dt.Columns.Add("tran_fee", typeof(decimal));
            dt.Columns.Add("sink_node_name", typeof(string));
            dt.Columns.Add("card_acceptor_name_loc", typeof(string));
            dt.Columns.Add("terminal_id", typeof(string));
            dt.Columns.Add("rsp_code_rsp", typeof(string));
            dt.Columns.Add("settle_currency_code", typeof(string));
            dt.Columns.Add("tran_currency_code", typeof(string));
            dt.Columns.Add("card_product", typeof(string));
            dt.Columns.Add("sponsor_bank", typeof(string));
            dt.Columns.Add("datetime_tran_gmt", typeof(DateTime));
            dt.Columns.Add("datetime_req", typeof(DateTime));
            dt.Columns.Add("datetime_rsp", typeof(DateTime));
            dt.Columns.Add("realtime_business_date", typeof(DateTime));
            dt.Columns.Add("recon_business_date", typeof(DateTime));
            dt.Columns.Add("terminal_owner", typeof(string));
            dt.Columns.Add("tran_reversed", typeof(string));

            return dt;
        }

        public static async Task<bool> BulkCopyToDest(CancellationToken cancellationToken, string query, int timeout, string destinationTable, string srcConn, string destCon)
        {
            //var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            //var connectionII = @"Server=(LocalDB)\BluePrismLocalDB;Initial Catalog=BluePrism;Trusted_Connection=True;";
            bool output = false;
            try
            {
                using (SqlConnection readConn = new SqlConnection(srcConn))
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destCon))
                    {

                        // Note that we are using the same cancellation token for calls to both connections\commands
                        // Also we can start both the connection opening asynchronously, and then wait for both to complete
                        logger.Info("Opening connection to Database");
                        Task openReadConn = readConn.OpenAsync(cancellationToken);
                        //Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                        await Task.WhenAll(openReadConn);


                        using (SqlCommand readCmd = new SqlCommand(query, readConn))
                        {
                            try
                            {
                                logger.Info("Reading Data from source Database");
                                readCmd.CommandTimeout = timeout;
                                SqlDataReader reader = await readCmd.ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken);

                                logger.Info("Writing Data to destination Database");
                                bulkCopy.DestinationTableName = destinationTable;
                                bulkCopy.BulkCopyTimeout = timeout;
                                bulkCopy.WriteToServer(reader);
                                output = true;
                            }
                            catch (Exception ex)
                            {

                                if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                                {
                                    string pattern = @"\d+";
                                    Match match = Regex.Match(ex.Message.ToString(), pattern);
                                    var index = Convert.ToInt32(match.Value) - 1;

                                    FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                                    var sortedColumns = fi.GetValue(bulkCopy);
                                    var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                                    FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                                    var metadata = itemdata.GetValue(items[index]);

                                    var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                    var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                    logger.Info(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                                }

                                output = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }
            return output;
        }

        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                logger.Info("Found an existing file.......");
                logger.Info("Deleting existing file from location.........");
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result.ToString());
            }
            else
            {
                File.WriteAllText(downloadPath, result.ToString());
            }
        }

    }
}
