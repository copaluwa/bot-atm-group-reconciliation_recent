﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexcubeExtraction
{
	//[Table("ATM_FLEXCUBE_GROUP_ENG")]
	[Table("ATM_FLEXCUBE_GROUP")]
   public  class DataModel
    {
		[Key]
		public long ID { get; set;}
		public DateTime? TRN_DT { get; set;}
		public DateTime? VALUE_DT { get; set;}
		public string INTERNAL_REFERENCE { get; set;}
		public string RRN { get; set;}
		public string EXTERNAL_REFERENCE { get; set;}
		public string NARRATION { get; set;}
		public decimal? LCY_AMOUNT { get; set;}
		public decimal? FCY_AMOUNT { get; set;}
		public string DRCR_IND { get; set;}
		public decimal? ABS_AMOUNT { get; set;}
		public decimal? ABS_FCY_AMOUNT { get; set;}
		public decimal? ACY_AMOUNT { get; set; }
		public string ABS_DRCR_IND { get; set;}
		public string MAKER { get; set;}
		public string AUTHORIZER { get; set;}
		public string FROM_ACC { get; set;}
		public string MSG_TYPE { get; set;}
		public string OFFUS_ONUS { get; set;}
		public string PAN { get; set;}
		public string RESP_CODE { get; set;}
		public string STAN { get; set;}
		public string TERM_ID { get; set;}
		public string TRN_REF_NO { get; set;}
		public string TXN_AMT { get; set;}
		public string XREF { get; set;}
		public string AC_BRANCH { get; set;}
		public string AC_NO { get; set;}
		public string AC_CCY { get; set;}
		public string RELATED_CUSTOMER { get; set;}
		public DateTime? SAVE_TIMESTAMP { get; set;}
		public string TERM_ADDR { get; set;}
		public string CLUSTER { get; set;}
		public string EJMatch { get; set; }
		public string ConfirmPresented { get; set; }
		public string ConfirmTaken { get; set; }
        public string ConfirmStaked { get; set; }


    }
}
