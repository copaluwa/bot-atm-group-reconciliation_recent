﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using Oracle.ManagedDataAccess.Client;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Data;

namespace FlexcubeExtraction
{
    public class Operations
    {
        private string connectionString;
        static Logger logger = new Logger();
        public Operations(string _connectionString)
        {
            connectionString = _connectionString;
        }
        public async Task<IEnumerable<AcctInfo>> GetAccounts(string cluster)
        {
            try
            {
                var queryScript = AppDomain.CurrentDomain.BaseDirectory + "SQL\\GetAccountNumbersPerCluster.sql";
              
                var query = File.ReadAllText(queryScript);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    var accts = await connection.QueryAsync<AcctInfo>(query, new { Cluster = cluster });
                    logger.Info($"Reading Accounts for {cluster} cluster completed");
                    return accts;
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while reading Accounts for {cluster} cluster");
                logger.Error(ex);
                return null;
            } 
        }

        public async Task<List<string>> SplitDataByRange(IEnumerable<AcctInfo> accts)
        {
            int range = Convert.ToInt32("range".GetKeyValue());
            List<string> accountsForExtraction = new List<string>();
            int count = 1;
            logger.Info($"plit by range started");
            if (accts.Any())
            {
                StringBuilder sb = new StringBuilder();
                int totalItems = accts.Count();
                int rangeCounter = 1;
                //int arrayLength = 0;
                //arrayLength = totalItems / range;
                //var mod = totalItems % range;
                //if (mod != 0)
                //    arrayLength = arrayLength + 1;
                foreach (var account in accts)
                {
                    if (rangeCounter == 1)
                        sb.Append($"'{account.ACCT}'");
                    else
                        sb.Append($",'{account.ACCT}'");
                    if(count % range == 0)
                    {
                        //start task
                        Console.WriteLine("Thread ");
                        Console.WriteLine(sb.ToString());
                        accountsForExtraction.Add(sb.ToString());
                        sb = new StringBuilder();
                        rangeCounter = 0;
                    }
                    if(count == totalItems)
                    {
                        Console.WriteLine("Thread ");
                        Console.WriteLine(sb.ToString());
                        accountsForExtraction.Add(sb.ToString());
                        break;
                    }
                    ++count;
                    ++rangeCounter;
                }
                
            }
            return accountsForExtraction;
        }

        public  async Task<bool> BulkCopyToDest2(CancellationToken cancellationToken, string query, int timeout, string destinationTable, string srcConn, string destCon)
        {

            //task.Add(Task.Run(() => ops.BulkCopyToDest2(CancellationToken.None, query, timeOut, destinationTable,
                           //inputParams.ConnectionString, destinationConnection)));


            //var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            //var connectionII = @"Server=(LocalDB)\BluePrismLocalDB;Initial Catalog=BluePrism;Trusted_Connection=True;";
            bool output = false;
            try
            {
                using (OracleConnection readConn = new OracleConnection(srcConn))
                {
                    using (SqlConnection bulkCopy = new SqlConnection(destCon))
                    {

                        // Note that we are using the same cancellation token for calls to both connections\commands
                        // Also we can start both the connection opening asynchronously, and then wait for both to complete
                        logger.Info("Opening connection to Database");
                        Task openReadConn = readConn.OpenAsync(cancellationToken);
                        //Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                        await Task.WhenAll(openReadConn);
                        DateTime? nodate = null;

                        OracleCommand readCmd = new OracleCommand(query, readConn);
                        try
                        {
                            var sourceData = new DataSet();
                            logger.Info("Reading Data from source Database");
                            readCmd.CommandTimeout = timeout;
                            var reader = readCmd.ExecuteReader(CommandBehavior.SequentialAccess);
                            reader.FetchSize = reader.RowSize * 1000;
                            logger.Info($"{reader.FetchSize}");
                            //var dataAdapter = new OracleDataAdapter(readCmd);
                            //var test2 = reader.GetInt64(0);

                            //dataAdapter.Fill(sourceData,"Transactions");
                            //sourceData.Columns.Add();
                            //for (int i = 0; i < reader.FieldCount; i++)
                            //{
                            //    sourceData.Columns.Add(reader.GetName(i), reader.GetFieldType(i));
                            //    var columnName = reader.GetName(i);
                            //    var dotNetType = reader.GetFieldType(i);
                            //    var sqlType = reader.GetDataTypeName(i);

                            //    // bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, typeof(DataModel).GetProperty(columnName).Name));
                            //}

                            //foreach (DataRow reader in sourceData.Tables["Transactions"].Rows)
                            while (reader.Read())
                            {
                                logger.Info("Mapping Oracle to SQL started");
                                int i = 0;
                                var model = new DataModel();
                                model.ABS_AMOUNT = reader["ABS_AMOUNT"] != DBNull.Value ? (decimal)reader["ABS_AMOUNT"] : 0;
                                logger.Info($"ABS AMOUNT is '{model.ABS_AMOUNT}'");
                                model.ABS_FCY_AMOUNT = reader["ABS_FCY_AMOUNT"] != DBNull.Value ? (decimal)reader["ABS_FCY_AMOUNT"] : 0;
                                model.ABS_DRCR_IND = reader["ABS_DRCR_IND"] != DBNull.Value ? (string)reader["ABS_DRCR_IND"] : "";
                                model.AC_BRANCH = reader["AC_BRANCH"] != DBNull.Value ? (string)reader["AC_BRANCH"] : "";
                                model.AC_CCY = reader["AC_CCY"] != DBNull.Value ? (string)reader["AC_CCY"] : "";
                                model.AC_NO = reader["AC_NO"] != DBNull.Value ? (string)reader["AC_NO"] : "";
                                model.AUTHORIZER = reader["AUTHORIZER"] != DBNull.Value ? (string)reader["AUTHORIZER"] : "";
                                model.CLUSTER = reader["CLUSTER"] != DBNull.Value ? (string)reader["CLUSTER"] : "";
                                model.DRCR_IND = reader["DRCR_IND"] != DBNull.Value ? (string)reader["DRCR_IND"] : "";
                                model.EXTERNAL_REFERENCE = reader["EXTERNAL_REFERENCE"] != DBNull.Value ? (string)reader["EXTERNAL_REFERENCE"] : "";
                                model.FROM_ACC = reader["FROM_ACC"] != DBNull.Value ? (string)reader["FROM_ACC"] : "";
                                model.INTERNAL_REFERENCE = reader["INTERNAL_REFERENCE"] != DBNull.Value ? (string)reader["INTERNAL_REFERENCE"] : "";
                                model.LCY_AMOUNT = reader["LCY_AMOUNT"] != DBNull.Value ? (decimal)reader["LCY_AMOUNT"] : 0;
                                model.FCY_AMOUNT = reader["FCY_AMOUNT"] != DBNull.Value ? (decimal)reader["FCY_AMOUNT"] : 0;
                                model.MAKER = reader["MAKER"] != DBNull.Value ? (string)reader["MAKER"] : "";
                                model.MSG_TYPE = reader["MSG_TYPE"] != DBNull.Value ? (string)reader["MSG_TYPE"] : "";
                                model.NARRATION = reader["NARRATION"] != DBNull.Value ? (string)reader["NARRATION"] : "";
                                model.OFFUS_ONUS = reader["OFFUS_ONUS"] != DBNull.Value ? (string)reader["OFFUS_ONUS"] : "";
                                model.PAN = reader["PAN"] != DBNull.Value ? (string)reader["PAN"] : "";
                                model.RELATED_CUSTOMER = reader["RELATED_CUSTOMER"] != DBNull.Value ? (string)reader["RELATED_CUSTOMER"] : "";
                                model.RESP_CODE = reader["RESP_CODE"] != DBNull.Value ? (string)reader["RESP_CODE"] : "";
                                model.RRN = reader["RRN"] != DBNull.Value ? (string)reader["RRN"] : "";
                                model.SAVE_TIMESTAMP = reader["SAVE_TIMESTAMP"] != DBNull.Value ? (DateTime?)reader["SAVE_TIMESTAMP"] : nodate;
                                model.STAN = reader["STAN"] != DBNull.Value ? (string)reader["STAN"] : "";
                                model.TERM_ADDR = reader["TERM_ADDR"] != DBNull.Value ? (string)reader["TERM_ADDR"] : "";
                                model.TERM_ID = reader["TERM_ID"] != DBNull.Value ? (string)reader["TERM_ID"] : "";
                                model.TRN_DT = reader["TRN_DT"] != DBNull.Value ? (DateTime)reader["TRN_DT"] : nodate;
                                model.TRN_REF_NO = reader["TRN_REF_NO"] != DBNull.Value ? (string)reader["TRN_REF_NO"] : "";
                                model.TXN_AMT = reader["TXN_AMT"] != DBNull.Value ? (string)reader["TXN_AMT"] : "";
                                model.VALUE_DT = reader["VALUE_DT"] != DBNull.Value ? (DateTime)reader["VALUE_DT"] : nodate;
                                model.XREF = reader["XREF"] != DBNull.Value ? (string)reader["XREF"] : "";
                                model.ACY_AMOUNT = model.FCY_AMOUNT != 0 ? model.FCY_AMOUNT : model.LCY_AMOUNT;
                                //model. = reader["XREF"] != DBNull.Value ? (string)reader["XREF"] : "";
                                // sourceData.Rows.Add(dataRow);
                                logger.Info("about inserting to destination");
                                i += 1;

                                await bulkCopy.InsertAsync(model);
                                logger.Info($"bulk insert Completed for {i}");

                            }

                            logger.Info("Writing Data to destination Database");

                            output = true;
                        }
                        catch (Exception ex)
                        {

                            if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                            {
                                string pattern = @"\d+";
                                Match match = Regex.Match(ex.Message.ToString(), pattern);
                                var index = Convert.ToInt32(match.Value) - 1;

                                FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                                var sortedColumns = fi.GetValue(bulkCopy);
                                var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                                FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                                var metadata = itemdata.GetValue(items[index]);

                                var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                logger.Info(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                            }
                            logger.Info("An error occured, see Error log for deatils");
                            logger.Error(ex);
                            output = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }
            return output;
        }

        public static async Task<bool> BulkCopyToDest(CancellationToken cancellationToken, string query, int timeout, string destinationTable, string srcConn, string destCon)
        {
            //var connection = @"Server=ESC2581\SQLEXPRESS;Initial Catalog=RPA;Trusted_Connection=True;";
            //var connectionII = @"Server=(LocalDB)\BluePrismLocalDB;Initial Catalog=BluePrism;Trusted_Connection=True;";
            bool output = false;
            try
            {
                using (OracleConnection readConn = new OracleConnection(srcConn))
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destCon))
                    {

                        // Note that we are using the same cancellation token for calls to both connections\commands
                        // Also we can start both the connection opening asynchronously, and then wait for both to complete
                        logger.Info("Opening connection to Database");
                        Task openReadConn = readConn.OpenAsync(cancellationToken);
                        //Task openWriteConn = writeConn.OpenAsync(cancellationToken);
                        await Task.WhenAll(openReadConn);


                        using (OracleCommand readCmd = new OracleCommand(query, readConn))
                        {
                            try
                            {
                                var sourceData = new DataTable();
                                logger.Info("Reading Data from source Database");
                                readCmd.CommandTimeout = timeout;
                                var reader = await readCmd.ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken);
                                //var dataAdapter = new OracleDataAdapter(readCmd);

                                //dataAdapter.Fill(sourceData);
                                sourceData.Columns.Add();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    sourceData.Columns.Add(reader.GetName(i), reader.GetFieldType(i));
                                    var columnName = reader.GetName(i);
                                    var dotNetType = reader.GetFieldType(i);
                                    var sqlType = reader.GetDataTypeName(i);

                                    // bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, typeof(DataModel).GetProperty(columnName).Name));
                                }

                                while (await reader.ReadAsync() != false)
                                {
                                    DataRow dataRow = sourceData.NewRow();
                                    for (int j = 0; j < reader.FieldCount; j++)
                                    {
                                        switch (reader.GetFieldType(j).Name)
                                        {
                                            case "DateTime":
                                                dataRow[reader.GetName(j)] = !reader.IsDBNull(j) ? reader.GetDateTime(j) : DateTime.Now.Date;
                                                break;
                                            case "Decimal":
                                                dataRow[reader.GetName(j)] = !reader.IsDBNull(j) ? reader.GetDecimal(j) : 0;
                                                break;
                                            case "String":
                                                dataRow[reader.GetName(j)] = !reader.IsDBNull(j) ? reader.GetString(j) : "";
                                                break;
                                            default:
                                                dataRow[reader.GetName(j)] = !reader.IsDBNull(j) ? reader.GetString(j) : "";
                                                break;
                                        }

                                    }
                                    sourceData.Rows.Add(dataRow);
                                    ;
                                }

                                logger.Info("Writing Data to destination Database");
                                bulkCopy.DestinationTableName = destinationTable;
                                bulkCopy.BulkCopyTimeout = timeout;
                                bulkCopy.EnableStreaming = true;
                                bulkCopy.WriteToServer(sourceData);
                                output = true;
                            }
                            catch (Exception ex)
                            {

                                if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                                {
                                    string pattern = @"\d+";
                                    Match match = Regex.Match(ex.Message.ToString(), pattern);
                                    var index = Convert.ToInt32(match.Value) - 1;

                                    FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                                    var sortedColumns = fi.GetValue(bulkCopy);
                                    var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                                    FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                                    var metadata = itemdata.GetValue(items[index]);

                                    var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                    var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                                    logger.Info(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                                }

                                output = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }
            return output;
        }

    }

    public class AcctInfo
    {
        public string ACCT { get; set; }
    }
}
