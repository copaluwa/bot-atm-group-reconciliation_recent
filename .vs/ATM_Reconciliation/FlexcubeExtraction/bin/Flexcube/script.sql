﻿SELECT trn_dt,
       value_dt,
       b.trn_ref_no internal_reference,
       rrn AS rrn,
       CASE
           WHEN rrn <> external_ref_no
           THEN
               external_ref_no || ' ' || 'RRN ' || rrn
           ELSE
               'RRN ' || rrn
       END
           AS external_reference,
          SUBSTR (pan, 1, 6)
       || '******'
       || SUBSTR (pan, 13, 4)
       || ' '
       || stan
       || ' '
       || CASE
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '12'
              THEN
                     '12'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '13'
              THEN
                     '01'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                 || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '14'
              THEN
                     '02'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '15'
              THEN
                     '03'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '16'
              THEN
                     '04'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '17'
              THEN
                     '05'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '18'
              THEN
                     '06'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '19'
              THEN
                     '07'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '20'
              THEN
                     '08'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '21'
              THEN
                     '09'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '22'
              THEN
                     '10'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '23'
              THEN
                     '11'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              ELSE
                     SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'AM'
                  || ' '
                  || msg_type
          END
       || ''
       || ' '
       || term_addr
           AS narration,
       lcy_amount,
       drcr_ind,
       ABS (lcy_amount) abs_amount,
       CASE
           WHEN SUBSTR (lcy_amount, 1, 1) = '-' AND drcr_ind = 'D' THEN 'C'
           WHEN SUBSTR (lcy_amount, 1, 1) = '-' AND drcr_ind = 'C' THEN 'D'
           ELSE drcr_ind
       END
           AS abs_drcr_ind,
       user_id AS maker,
       auth_id AS authorizer,
    a.FROM_ACC,
    a.MSG_TYPE,
    a.OFFUS_ONUS,
    a.PAN,
    a.RESP_CODE,
    a.STAN,
    a.TERM_ID,
    b.TRN_REF_NO,
    a.TXN_AMT,
    a.XREF,
    b.AC_BRANCH,
    b.AC_NO,
    b.AC_CCY,
    b.RELATED_CUSTOMER,
    b.SAVE_TIMESTAMP,
    a.TERM_ADDR,
'{CLUSTER}' as "CLUSTER"
  FROM {Prefix}.swtb_txn_hist a, {Prefix}.acvw_all_ac_entries_new b
  WHERE     
      trn_dt = '{ReconEndDate}'
      AND a.trn_ref_no = b.trn_ref_no(+)
      and nvl(ib,'N') <> 'Y'
      AND ac_no in 
      ({ACCTS})
  UNION ALL
  SELECT trn_dt,
       value_dt,
       b.trn_ref_no internal_reference,
       rrn AS rrn,
       CASE
           WHEN rrn <> external_ref_no
           THEN
               external_ref_no || ' ' || 'RRN ' || rrn
           ELSE
               'RRN ' || rrn
       END
           AS external_reference,
          SUBSTR (pan, 1, 6)
       || '******'
       || SUBSTR (pan, 13, 4)
       || ' '
       || stan
       || ' '
       || CASE
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '12'
              THEN
                     '12'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '13'
              THEN
                     '01'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                 || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '14'
              THEN
                     '02'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '15'
              THEN
                     '03'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '16'
              THEN
                     '04'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '17'
              THEN
                     '05'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '18'
              THEN
                     '06'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '19'
              THEN
                     '07'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '20'
              THEN
                     '08'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '21'
              THEN
                     '09'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '22'
              THEN
                     '10'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              WHEN SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2) = '23'
              THEN
                     '11'
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'PM '
              ELSE
                     SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 5, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 7, 2)
                  || ':'
                  || SUBSTR ( (LPAD (trans_dt_time, 10, 0)), 9, 2)
                  || ''
                  || 'AM'
                  || ' '
                  || msg_type
          END
       || ''
       || ' '
       || term_addr
           AS narration,
       lcy_amount,
       drcr_ind,
       ABS (lcy_amount) abs_amount,
       CASE
           WHEN SUBSTR (lcy_amount, 1, 1) = '-' AND drcr_ind = 'D' THEN 'C'
           WHEN SUBSTR (lcy_amount, 1, 1) = '-' AND drcr_ind = 'C' THEN 'D'
           ELSE drcr_ind
       END
           AS abs_drcr_ind,
       user_id AS maker,
       auth_id AS authorizer,
          a.FROM_ACC,
     a.MSG_TYPE,
    a.OFFUS_ONUS,
    a.PAN,
    a.RESP_CODE,
    a.STAN,
    a.TERM_ID,
    b.TRN_REF_NO,
    a.TXN_AMT,
    a.XREF,
    b.AC_BRANCH,
    b.AC_NO,
    b.AC_CCY,
    b.RELATED_CUSTOMER,
    b.SAVE_TIMESTAMP,
    a.TERM_ADDR,
'{CLUSTER}' as "CLUSTER"
  FROM {Prefix}.swtb_txn_log a, {Prefix}.acvw_all_ac_entries_new b
  WHERE     
      trn_dt = '{ReconEndDate}'
      AND a.trn_ref_no = b.trn_ref_no(+)
      and nvl(ib,'N') <> 'Y'
      AND ac_no in 
      ({ACCTS})