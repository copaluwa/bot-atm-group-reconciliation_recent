﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlexcubeExtraction
{
    public static class Extention
    {
        public static string GetKeyValue(this string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
