﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace FlexcubeExtraction
{
    class Program
    {
        static Logger logger = new Logger();
        static async Task  Main(string[] args)
        {
            //Load connections;


            //my changes
            logger.Info("Extraction Started");
            logger.Info("Loading Configuration");
            var destinationConnection = "destConnectionString".GetKeyValue();
            Operations ops = new Operations(destinationConnection);
           
            var sourceConnection = "sourceConnectionString".GetKeyValue();

            var destinationTable = "destinationTable".GetKeyValue();
            int timeOut = Convert.ToInt32("timeout".GetKeyValue());
            string resultPath = "resultPath".GetKeyValue();
            bool outcome = false;
            string baseFolder = AppDomain.CurrentDomain.BaseDirectory;
            string queryScript = File.ReadAllText($"{baseFolder}\\script.sql");

            InputParams inputParams = new InputParams(args);

            queryScript = queryScript.Replace("{ReconStartDate}", inputParams.ReconStartDate);
            queryScript = queryScript.Replace("{ReconEndDate}", inputParams.ReconEndDate);
            queryScript = queryScript.Replace("{CLUSTER}", inputParams.Cluster);
            queryScript = queryScript.Replace("{Prefix}", inputParams.Prefix);
            queryScript = queryScript.Replace("{NarrationPrefix}", inputParams.NarrationPrefix);
            try
            {
                //var output = await ops.GetAccounts("AWA");
                var output = await ops.GetAccounts(inputParams.Cluster);
                var accounts = await ops.SplitDataByRange(output);
                
                if (accounts.Any())
                {
                    List<Task> task = new List<Task>();

                    foreach(var account in accounts)
                    {
                        var query = queryScript.Replace("{ACCTS}", account);
                        logger.Info($"account query created");
                        task.Add(Task.Run( () =>  ops.BulkCopyToDest2(CancellationToken.None, query, timeOut, destinationTable,
                            inputParams.ConnectionString, destinationConnection)));
                    }
                    Task.WaitAll(task.ToArray());
                    
                    if(task.Any(t => t.IsCompleted))
                    {
                        outcome = true;
                    }
                    foreach(var t in task)
                    {
                        logger.Info(String.Format("Task #{0} Status {1}, Output Result #{2}.",
                                              t.Id, t.Status, t.IsCompleted));
                    }
                    

                }

            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }

            WriteOutput(outcome, resultPath);
            //Console.ReadKey();
        }

       

      
        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                logger.Info("Found an existing file.......");
                logger.Info("Deleting existing file from location.........");
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result.ToString());
            }
            else
            {
                File.WriteAllText(downloadPath, result.ToString());
            }
        }

        


    }
    
}
